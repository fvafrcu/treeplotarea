Dear CRAN Team, dear Victoria,
responding to Victoria Wimmer's comments on my first submission of treePlotArea
1.0.0, this is a resubmission of treePlotArea 1.0.0.
I have 
* added Bernhard Bösch, my former colleague here at the Department of Biometrics
  and Informatics at the Forest Research Institute of Baden-Wuerttemberg, to
  authors@R.
* Bitterlich's publication:
  I have cited this publication in the vignette and the help page for the
  package (man/treePlotArea-package.Rd) with full reference.
  I have added its full reference to DESCRIPTION as this publication
  (written in german, from the first half of the last century, with a journal 
  that never had any peer review and change it's name a couple of times)
  has no doi, isbn, url whatsoever.
* Grosenbaugh's publication:
  I have cited this publication in the vignette and the help page for the
  package (man/treePlotArea-package.Rd) with full reference.
  I have added its url to DESCRIPTION as its doi does not seem to be valid. 
  I reported the broken doi to Oxford University Press.

Best, 
Dominik


# Package treePlotArea 1.0.0

Reporting is done by packager version 1.15.0


## Test environments
- R Under development (unstable) (2022-06-23 r82516)
   Platform: x86_64-pc-linux-gnu (64-bit)
   Running under: Devuan GNU/Linux 4 (chimaera)
   0 errors | 0 warnings | 1 note 
- gitlab.com
  R version 4.2.1 (2022-06-23)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Ubuntu 20.04.4 LTS
  Package: treePlotArea
  Version: 1.0.0
  Status: 2 WARNINGs, 1 NOTE

- win-builder (devel)
Status: 1 NOTE
R Under development (unstable) (2022-07-08 r82567 ucrt)


## Local test results
- RUnit:
    treePlotArea_unit_test - 4 test functions, 0 errors, 0 failures in 9 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 1 ]
- tinytest:
    All ok, 3 results (1.3s)
- Coverage by covr:
    treePlotArea Coverage: 96.88%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for get_correction_factors by 5.
- lintr:
    found 0 lints in 19137 lines of code (a ratio of 0).
- cleanr:
    found 0 dreadful things about your code.
- codetools::checkUsagePackage:
    found 2 issues.
- devtools::spell_check:
    found 18 unkown words.
