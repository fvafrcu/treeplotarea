Dear CRAN Team,
I am a forest inventory statistican at the forest research institute of
Baden-Wuerttemberg.  Currently I work with people from the federal Thuenen
Institute on the current German national forest inventory 2022.  

The German national forest inventory uses a probability proportional to size
sampling method (called angle count sampling, invented by Bitterlich in 1948).
Each tree gets its own sample plot proportional to its squared diameter. 
When trees are located near stand boundaries, their individual sample plots may
get intersected by those boundaries, and their effective sample plot sizes need
to be corrected.

treePlotArea is a package that calculates correction factors for trees whose
sample plots are intersected by stand boundaries. 
It may not be of much interest to a wider public, but it is of importance to the
official statistics of the German national forest inventory.
And we are probably going to need it with the next German national forest
inventory in 2032. As it relies on third party packages (namely 'sf'), we would
be grateful to have it published on CRAN to maximise the probability of the
package running out of the box in ten years time.
Please consider uploading to CRAN.

Best, 
Andreas Dominik Cullmann


# Package treePlotArea 1.0.0

Reporting is done by packager version 1.14.1


## Test environments
- R Under development (unstable) (2022-06-23 r82516)
   Platform: x86_64-pc-linux-gnu (64-bit)
   Running under: Devuan GNU/Linux 4 (chimaera)
   0 errors | 0 warnings | 1 note 
- gitlab.com
  R version 4.2.1 (2022-06-23)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Ubuntu 20.04.4 LTS
  Package: treePlotArea
  Version: 1.0.0
  Status: 2 WARNINGs, 1 NOTE

- win-builder (devel)
  Status: 1 NOTE
  R Under development (unstable) (2022-07-06 r82554 ucrt)

## Local test results
- RUnit:
    treePlotArea_unit_test - 4 test functions, 0 errors, 0 failures in 9 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 1 ]
- tinytest:
    All ok, 3 results (1.4s)
- Coverage by covr:
    treePlotArea Coverage: 96.88%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for get_correction_factors by 5.
- lintr:
    found 0 lints in 19126 lines of code (a ratio of 0).
- cleanr:
    found 0 dreadful things about your code.
- codetools::checkUsagePackage:
    found 2 issues.
- devtools::spell_check:
    found 5 unkown words.
