- pass counting_factor from get_correction_factors down get_correction_factor.
  check_tree to get_boundary_radius&szlig;
- pass counting_factor and area to each call of get_r_max(). Via an option?
- redesign check_boundaries: goes_through_origin should be only on thing in it.
  The message should come from goes_through_origin?
- pass number of n-gon down to circle2polygon via option?
- conv function to check on the validity of data boundaries/angle_counts?
- try solve the flexing toward or away differently?
