Dear CRAN Team,
this is a resubmission of package 'treePlotArea'. I have added the following changes:

* Added argument `is_ti_round` defaulting to `TRUE` to 
 `get_corrections_factors()` to allow for rounding the tree's boundary circle's 
 radius to the unit (i.e. [cm]) as done by Thuenen Institute?


Please upload to CRAN.
Best, Andreas Dominik

# Package treePlotArea 2.1.0

Reporting is done by packager version 1.15.2.9000


## Test environments
- R Under development (unstable) (2024-02-25 r85986)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 5 (daedalus)
   0 errors | 0 warnings | 1 note 
- gitlab.com
  R version 4.3.3 (2024-02-29)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Ubuntu 22.04.4 LTS
  Package: treePlotArea
  Version: 2.1.0
  Status: 2 WARNINGs, 1 NOTE
- win-builder (devel)

## Local test results
- RUnit:
    treePlotArea_unit_test - 8 test functions, 0 errors, 0 failures in 21 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 1 ]
- tinytest:
    All ok, 6 results (1.7s)
- Coverage by covr:
    treePlotArea Coverage: 86.27%

## Local meta results
- Cyclocomp:
     Exceeding maximum cyclomatic complexity of 10 for get_correction_factors by 5.
     Exceeding maximum cyclomatic complexity of 10 for goes_through_origin by 1.
- lintr:
    found 29 lints in 19519 lines of code (a ratio of 0.0015).
- cleanr:
    found 4 dreadful things about your code.
- codetools::checkUsagePackage:
    found 2 issues.
- devtools::spell_check:
    found 23 unkown words.
