// GrenzkreisDll.h : Haupt-Header-Datei f�r die DLL GRENZKREISDLL
//

#if !defined(AFX_GRENZKREISDLL_H__26365F3B_D681_42D2_B0A6_2D4D50DA0D8E__INCLUDED_)
#define AFX_GRENZKREISDLL_H__26365F3B_D681_42D2_B0A6_2D4D50DA0D8E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// Hauptsymbole


/////////////////////////////////////////////////////////////////////////////
// CGrenzkreisDllApp
// Siehe GrenzkreisDll.cpp f�r die Implementierung dieser Klasse
//

class CGrenzkreisDllApp : public CWinApp
{
public:
	CGrenzkreisDllApp();

// �berladungen
	// Vom Klassenassistenten generierte �berladungen virtueller Funktionen
	//{{AFX_VIRTUAL(CGrenzkreisDllApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CGrenzkreisDllApp)
		// HINWEIS - An dieser Stelle werden Member-Funktionen vom Klassen-Assistenten eingef�gt und entfernt.
		//    Innerhalb dieser generierten Quelltextabschnitte NICHTS VER�NDERN!
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // !defined(AFX_GRENZKREISDLL_H__26365F3B_D681_42D2_B0A6_2D4D50DA0D8E__INCLUDED_)
