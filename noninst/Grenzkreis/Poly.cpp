/** @file Poly.cpp
* Diese Datei enthaelt die eigentlichen Algorithmen f�r Fl�chenberechnung von Kreisen, die von    *
* geraden und/oder geknickten Linien geschnitten werden (entwickelt aus BI-Redesign) 
* - weiterentwickel Boesch 10.11.05  BWI
* - �nderung: verwendung des GPC Clipp algorithmusses 20100601
* - Fehlerr�ckspr�nge und Korrekturfaktor eingef�gt 20101203
* @author Boesch
**************************************************************************************/

#include "stdafx.h"
#include "math.h"
#include "poly.h"
#include "gpc.h"



double feld[maxfeld + 1][5 + 1];
double tx,ty;				// Transformationsgr��en wenn Grenzkreis nicht im Ursprung	
int start = 0;

int frei = 40, nEck=40;     // nEck ist Anzahl Ecken des Polygonkreises bei Start
static CString  GLTyp[2 + 1];
static double fGGerade[4+1][4+1]; int nGGerade;

double pi = 3.141592654;

// ###################################################################################################################################################



void  GrenzenSet::BerechnePolygonflaeche(float fKE,float fKA, float fGrenze1[][3],float fGrenze2[][3], float Radius, double& Flaeche, double &dKfaktor, short &iErr)
/**Die Funktion G_Kreisfl�che_BWI berechnet die Fl�che des Kreises mit Mittelpunkt Entf, 
* Azimut mit den Grenzen Grenze1, Grenze2. Die Sektorspitze liegt im zweiten Feld Grenze1(1,.) 
* bzw. Grenze2(1,.) (Indizierung beginnt bei 0!).  Dieses Feld  bleibt frei, wenn es sich um eine 
* Gerade handelt.
* Die Prozedur kann verwendet werden f�r Probekreise (Entf=0.0, fAzi=0.0, Radius=Radius des Probekreises) 
* oder f�r den Probekreis um den Einzelbaum. In diesem Falle entspricht Entf, Azimut den Koordinaten des 
* Einzelbaumes, der Radius dem Wert (Bhd/(2*Wurzel(Z�hlbreite)). 

* Entf:	 	Koordinaten des Einzelbaumes (m), bei Probekreis =0
* Azimut:	Koordinate des Einzelbaumes (gon), bei Probekreis=0
* fGrenze.[0][i]= i=0-2, Azimut  fGrenze.[1][i]; i=0-2; Entfernung
*
* @param Grenze1(0,0)	Azimut (gon) erster Punkt		
* @param Grenze1(1,0)	Entfernung (m) erster Punkt		
* @param Grenze1(0,1) 	Azimut (gon) zweiter Punkt		 /Spitze bei Knick
* @param Grenze1(1,1) 	Entfernung (m) zweiter Punkt	/ Spitze bei Knick
* @param Grenze1(0,2) 	Azimut (gon) dritter Punkt
* @param Grenze1(1,2) 	Entfernung (m) dritter Punkt

* @param Grenze2(. ,. ) 	Azimut(gon) erster Punkt
* @param   	. 
* @param      	.
* @param Radius		Radius des Probekreises (m)

* Output: 	Funktionswert: Schnittfl�che [m�]
* @param Kfaktor: 	Fl�che voller Grenzkreis/Fl�che Geschnittener Grenzkreiskreis
* @param iErr: 		siehe 4.1

//------------------------------------------------------------------------------------------------*/
{
	int i,  di;
	bool nG[3];
 	double p0[6 + 1][2 + 1];	// kartes. Koordinaten der Grenzlinien mit Orginalwerten
	double Azi, ent;			// Hilfsvariablen f�r Azimut und Entfernung
	//double tx,ty;				// Transformationsgr��en wenn Grenzkreis nicht im Ursprung	
	double dFlaecheKreis;		//original Kreisfl�che
	CString Variante;
	float fZb=4.0;				// Z�hlbreite
	
	//==========================================================================================	
	
	start = 0;
	frei = nEck;   // Anzahl der Kreissektoren
	dKfaktor=0;
	Flaeche=0;
	iErr=0;
	//------------------------------------------------------------------------------------
	// Plausi1 auf Entf >0 fatale Fehler mit sofortigem R�cksprung
	for (i=1;i<=3;i++)
		{
		if (fGrenze1[1][i-1]<=0 && fGrenze1[0][i-1]!=0)
			goto ERREntf;

        if (fGrenze2[1][i-1]<=0 && fGrenze2[0][i-1]!=0)
			goto ERREntf;

		if(fGrenze1[0][i-1]<0 || fGrenze2[0][i-1]<0 || fGrenze2[0][i-1]>400 || fGrenze2[0][i-1]>400)
			goto ERRAz;
		} 
    // Radius <=0
	if(Radius<=0) goto ERRRad;

    // Sektorenden liegen aufeinander
    if (fGrenze1[1][0]>0 && fabs(fGrenze1[1][0]-fGrenze1[1][1])<0.000001 && fabs(fGrenze1[0][0]-fGrenze1[0][1])<0.000001)
		goto ERRSek;
    if (fGrenze1[1][0]>0 && fabs(fGrenze1[1][0]-fGrenze1[1][2])<0.000001 && fabs(fGrenze1[0][0]-fGrenze1[0][2])<0.000001)
		goto ERRSek;
    if (fGrenze1[1][1]>0 && fabs(fGrenze1[1][1]-fGrenze1[1][2])<0.000001 && fabs(fGrenze1[0][1]-fGrenze1[0][2])<0.000001)
		goto ERRSek;
 
	if (fGrenze2[1][0]>0 && fabs(fGrenze2[1][0]-fGrenze2[1][1])<0.000001 && fabs(fGrenze2[0][0]-fGrenze2[0][1])<0.000001)
		goto ERRSek;
    if (fGrenze2[1][0]>0 && fabs(fGrenze2[1][0]-fGrenze2[1][2])<0.000001 && fabs(fGrenze2[0][0]-fGrenze2[0][2])<0.000001)
		goto ERRSek;
    if (fGrenze2[1][1]>0 && fabs(fGrenze2[1][1]-fGrenze2[1][2])<0.000001 && fabs(fGrenze2[0][1]-fGrenze2[0][2])<0.000001)
		goto ERRSek;
	
	//----------------------------------------------------------------------------------------

	if (fKE>0) // Berechnung der Transformationsgr��en wenn Probekreis nicht im Zentrum Stichprobe
		{
		Azi = (500 - fKA) * pi / 200;	
		ent = fKE;  
		tx  = ent * cos(Azi);	// kartes. X-Wert
		ty  = ent * sin(Azi);
	    //Radius=Radius/2/((float)sqrt(fZb));            Radius muss selbst berechnet werden
	    }
	else
		{
		tx=0.0f;
		ty=0.0f;
		}


	//Grenzlinien-Koordinaten Nullsetzen
	// Grenze 1 ------------------------------------------------------------------------------
	for (i = 1; i <= 3; i++)
		{
		p0[i][1] = 0;	// kartes. X-Wert
		p0[i][2] = 0;	// kartes. Y-Wert
		Azi = (500 - fGrenze1[0][i-1]) * pi / 200;	// Transformation des eingelesenen Azimuts vom geogr. System
		ent = fGrenze1[1][i-1];                    // in das geometrische sowie von gon in rad
		if (ent>0)
			{
			p0[i][1] = ent * cos(Azi)- tx;	// kartes. X-Wert
			p0[i][2] = ent * sin(Azi)- ty;	// kartes. Y-Wert
			}
		}
	
	if(fGrenze1[1][2]>0)
		GLTyp[1]="S";        // Sektor oder
	else
		GLTyp[1]="G";        // Gerade
  
	// Grenze 2 ------------------------------------------------------------------------------
	for (i = 4; i <= 6; i++)
		{
		p0[i][1] = 0.0f;	// kartes. X-Wert
		p0[i][2] = 0.0f;	// kartes. Y-Wert
		Azi = (500 - fGrenze2[0][i-4]) * pi / 200;	// Transformation des eingelesenen Azimuts vom geogr. System
		ent = fGrenze2[1][i-4];                    // in das geometrische sowie von gon in rad
		
		if(ent>0)
			{
			p0[i][1] = ent * cos(Azi) - tx;	// kartes. X-Wert
			p0[i][2] = ent * sin(Azi) - ty;	// kartes. Y-Wert
			}
		}
	if(fGrenze2[1][2]>0)
		GLTyp[2]="S";
	else
		GLTyp[2]="G";

	//-------------------------------------------------------------------------------------
	
	// Plausi 2 Gleiche Punkte oder Abstand<10, Warnings, berechnungen gehen weiter
		{
		short itmpErr1=0,itmpErr2=0,i,j;

		for(i=1;i<=6;i++)
			{
			for(j=i+1;j<=6;j++)
				{
				if(abstand(0,0,p0[j][1],p0[j][2])>0.001) // Punkt belegt ?
					{
					if(p0[i][1]==p0[j][1] && p0[i][2]==p0[j][2])
						itmpErr1=1;
					if(abstand(p0[i][1],p0[i][2],p0[j][1],p0[j][2])<10)
						itmpErr2=1;
					}
				}
			}
		if(p0[1][1]==p0[4][1] && p0[2][1]==p0[5][1] && p0[3][1]==p0[6][1] && 
			p0[1][2]==p0[4][2] && p0[2][2]==p0[5][2] && p0[3][2]==p0[6][2])// Wenn beide Grenzen gleich Grenze 2 auf Null setzen
			{
			p0[4][1]=p0[5][1]=p0[6][1]=p0[4][2]=p0[5][2]=p0[6][2]=0;
			itmpErr1=1;
			}
		if(itmpErr2==1) iErr=-20;
		if(itmpErr1==1) iErr=-11;
		
		}
		//-------------------------------------------------------------------------------------
	
	
	
	// Grenzen in globales Geradenfeld schreiben
	grenzensort( p0); 
  
	//----------------------------------------------------------------------------------------------  
	nG[1]=(fGrenze1[1][0]>0);
	nG[2]= (fGrenze2[1][0]>0);  // Gibt es zwei Grenzen (Erste Entf. muss >0 sein)
	
	
	//**********************************************************************************************
	// ************** Hier beginnen die eigentlichen Berechnungen
	
	Polygoninit(Radius);
	dFlaecheKreis=PFlaeche(); // Originalfl�che des Kreises vor Clippen
	
	for (i = 1; i <= 2; i++)
		{
		if(nG[i])
			{
			di = (i - 1) * 3;
		
			if ( GLTyp[i] == "G" )
				ClippGerade(p0[1 + di][1], p0[1 + di][2], p0[2 + di][1], p0[2 + di][2]);
			else if ( GLTyp[i] == "S" )
				{
				//ClippSektor(p0[2 + di][1], p0[2 + di][2], p0[1 + di][1], p0[1 + di][2], p0[3 + di][1], p0[3 + di][2]);
		
				double (*tt)[3];
				tt=&p0[di];
				gpcclip(tt,feld,start);    // �bergabe an Clipp Algorithmus
				}
			}
		}       
	
	Flaeche = fabs(PFlaeche());
	dKfaktor= fabs(dFlaecheKreis/Flaeche);
	return;

	//#########################################################
	//Fehlerr�cksprung
	ERREntf:     // Entfernung null und Winkel nicht null
	iErr=-1;
    Flaeche=0;
	return;
	
	ERRAz:		// Winkel <0 oder >400
	iErr=-2;
	return;

	ERRRad:     //Radisu <=0
	iErr=-3;
	return;

	ERRSek: //Sektorenden liegen aufeinader -> Gerade
	iErr=-4;
	return;

}



//************************************************************************************************



int GrenzenSet::iLageBaum(double gx1, double gy1, double gx2, double gy2, double gx3, double gy3, double Bpx, double Bpy)
/**------------------------------------------------------------------------------------------------
* Funktion stellt fest, ob ein Baum mit den Koordinaten Bpx und Bpy au�erhalb von Grenzlinien liegt
*  bezogen auf den Ursprung (= Stichprobenmittelpunkt)
*  R�ckgabewert 1: Baum ist im Probekreis, 0: Baum ist au�erhalb
*  Es werden zwei F�lle unterschieden:
*  Die Grenzlinie ist eine Gerade (gx2, gxy ["Knickpunkt"]ist jeweils 0)
*  Die Grenzlinie ist geknickt
*  Erl�uterung: dieses Kriterium, Knickpunkt Koordinaten = 0 ist ausreichend, da verfahrensgem�� eine
* linie nicht durch den Stichprobenmittelpunkt gehen darf.
* Bem B�sch: kann inzwischen einfacher durch Punkt in Polygon gel�st werden, wenn man f�r das Polygon den 
* resultierenden Polygonzug nimmt
* @param author G. K�ndler
* @param date 02/2002\n
*---------------------------------------------------------------------------------------------------------------*/
	{
	double theta1, theta2, H;
	int retval=0;
	//================================================================================================
	
	//Grenzlinie ist gerade
	if ( (gx2 == 0) && (gy2 == 0) )
		{
    if(insideline(gx1, gy1, gx3, gy3, Bpx, Bpy) == insideline(gx1, gy1, gx3, gy3, 0, 0))
			retval=1;
		}
	else
		{
		//Grenzlinie ist geknickt
		theta1 = theta(gx2, gy2, gx1, gy1);
		theta2 = theta(gx2, gy2, gx3, gy3);
		
		if ( theta1 > theta2)
			{
			//Sektor ordnen
			H = theta2;
			theta2 = theta1;
			theta1 = H;
			}
		
		//"Vgl" muss berechnet werden, wobei in Function "insidesektor" der Parameter "Vergleich" = 0
		//gesetzt wird (K� / 26.02.02)
		if(insidesektor(theta1, theta2, gx2, gy2, 0, 0, 0)== insidesektor(theta1, theta2, gx2, gy2, Bpx, Bpy, 0))
			retval=1;
		}
	
  return retval;
	
	}
//**************************************************************************************



int GrenzenSet::insideline(double gx1, double gy1, double gx2, double gy2, double px, double py)
/** Berechnet ob Punkt px,py links oder rechts von Gerade durch die ersten 2 Punkte liegt
* verwende Vorzeichen des Vektorprodukts	
*------------------------------------------------------------------------------------------------*/
{
	double vx, vy, wx, wy;
	//==========================================================================================
	
	vx = gx2 - gx1;
	vy = gy2 - gy1;
	wx = px - gx1;
	wy = py - gy1;
	
	if ( (vx * wy - vy * wx) > 0 )
		return 1;
	
	return 0;
}


//*****************************************************************************************


void  GrenzenSet::ClippGerade(double gx1, double gy1, double gx2, double gy2)
/*Clippt alle Punkte des Polygons die nicht auf der Seite des Ursprunges liegen
*------------------------------------------------------------------------------------------------*/
{
	int test, test0, testn,schtest,ptest; 
	double xs, ys;
	int j,s;
	//================================================	
	
	test0 = insideline(gx1, gy1, gx2, gy2, 0, 0);
	j = start;
	
	while ( insideline(gx1, gy1, gx2, gy2, feld[j][1], feld[j][2]) != test0 )
		j = (int)feld[j][4];
	
	s = j;
	test = insideline(gx1, gy1, gx2, gy2, feld[j][1], feld[j][2]);
	
	do
		{
			j = (int)feld[j][4];
			testn = insideline(gx1, gy1, gx2, gy2, feld[j][1], feld[j][2]);
			if ( testn == test )
			{
				if ( testn != test0 )
				{
					if ( j == start )
						start = (int)feld[j][4];
					
					Loeschen((int)feld[j][3]);
				}
			}
			else
			{
				//Schnittgerade
				schtest=schnittpunkt(gx1, gy1, gx2, gy2, feld[j][1], feld[j][2], feld[(int)feld[j][3]][1], feld[(int)feld[j][3]][2], xs, ys);
				if (schtest == 0)
					{
                    double Abst,mx,my;
					ptest = Punkt_in_Gerade(gx1, gy1, gx2, gy2, feld[j][1], feld[j][2], Abst, mx, my);
					}
				if(!(schtest==0 && ptest<=0))
					{
					if ( testn == test0 )
						{
						feld[(int)feld[j][3]][1] = xs;
						feld[(int)feld[j][3]][2] = ys;
						}
					else
						einfuegen(j, xs, ys);
					}
			}

			
			test = testn;
		}
		while ( j != s );
}

//***********************************************************************************************


int  GrenzenSet::schnittpunkt(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double& xs, double& ys)
/**Diese Funktion bestimmt den Schnittpunkt (xs, ys) von zwei Geraden. 
* Gerade 1 ist durch die Punkte g1x1, g1y1, g1x2, g1y2 gegeben, 
* Gerade 2 durch g2x3, g2y3 und g2x4, g2y4. 
* Der R�ckgabewert ist 1 wenn ein Schnittpunkt existiert, in diesem Falle sind die Koordinaten 
* des Schnittpunktes (xs,ys). Wenn die Geraden parallel sind ist der R�ckgabewert 0.
* @autor Boesch\n
------------------------------------------------------------------------------------------------*/

{
	double del, rmu;
	//=================================================================
	xs=0;
	ys=0;
	del = (x1 - x2) * (y4 - y3) - (y1 - y2) * (x4 - x3);
	
	//wenn del= 0, Geraden Parallel
	if ( fabs(del) > 0.00001 )
		{
		rmu = ((y4 - y3) * (x4 - x2) - (x4 - x3) * (y4 - y2)) / del;
		xs = rmu * x1 + (1.0 - rmu) * x2;
		ys = rmu * y1 + (1.0 - rmu) * y2;
		return 1;
		}
	
	return 0;
}

//**********************************************************************************************

double  GrenzenSet::PFlaeche()
/**Berechnet die Fl�che innerhalb des Polygons
//------------------------------------------------------------------------------------------------*/

{
	int k, j;
	double f;
	//==================================================================
	
	k = 0;
	f = 0;
	j = start;
	
	do
		{
			k = k + 1;	// Test
			f = f + (feld[j][1] + feld[(int)feld[j][3]][1]) * (feld[j][2] - feld[(int)feld[j][3]][2]);
			j = (int)feld[j][4];
			//TRACE2("\n%f5.2 %f5.2",feld[j][1], feld[j][2]);
		}
		while ( (j != start) && (k <= maxfeld) );
		
		// Nur f�r Test
		//MsgBox ("<PFlaeche> meldet: " & Str$(k) & " Iterationen!")
		
		return fabs(f) / 2;
}


//***********************************************************************************************


double  GrenzenSet::theta(double p1x, double p1y, double p2x, double p2y)
/**Berechnet Pseudowinkel der Geraden durch die 2 Punkte
//------------------------------------------------------------------------------------------------*/
{
	double Dx, ax, dy, ay, t;
	//====================================================================
	
	Dx = p2x - p1x;
	ax = fabs(Dx);
	dy = p2y - p1y;
	ay = fabs(dy);
	t = (ay + ay);
	
	if ( t != 0)
		t = dy / (ax + ay);
	
	if ( Dx < 0 )
		t = 2 - t;
	else
		if ( dy < 0 )
			t = 4 + t;
		
		return t * 90;
}


//*******************************************************************************************


int  GrenzenSet::insidesektor(double phi1, double phi2, double pxz, double pyz, double x, double y, double vergleich)
/**Pr�ft ob Punkt x,phi1, y,phi2 bzgl. Drehpunkt pxz,pyz im gleichen Sektor wie Ursprung liegt
// Zuvor muss gepr�ft werden, ob Ursprung im Sektor liegt:
//Vergleich = 0, wenn Ursprung in Sektor liegt, Vergleich= 1, wenn Ursprung au�erhalb
// phi2 muss gr��er phi1 sein 
//R�ckgabewert = 1, wenn Punkt im gleichen Sektor sonst 0
//------------------------------------------------------------------------------------------------*/
	{  
		double eps, phi;
		int f;
		//=======================================================================
		
		eps = 0.00001;	// Urspr�nglich 0.0001
		phi = theta(pxz, pyz, x, y);
		f = 0;
		
		if ( ((vergleich == 1) && (phi >= phi1 - eps) && (phi <= phi2 + eps)) ||
			((vergleich == 0) && (phi >= phi2 - eps || phi <= phi1 + eps)) )
			f = 1;
		
		return f;
}

//*******************************************************************************************


void  GrenzenSet::ClippSektor(double px1, double py1, double px2, double py2, double px3, double py3)
/** L�scht alle Punkte des Polygons die nicht auf der Seite des Ursprunges liegen
// B�sch 19.10.05
//------------------------------------------------------------------------------------------------*/

{
	double  theta1, theta2, H, xs, ys;
	int j, jn, Spitze_im_Polygon ;
	double test, testg, testgn, test0, testn;
	double s, seingefuegt, loesch;
	//=====================================================================================
	
	//r = sqrt(px2 * px2 + py2 * py2);
	theta1 = theta(px2, py2, px1, py1);
	theta2 = theta(px2, py2, px3, py3);
	
	if ( theta1 > theta2)
		{
		// Sektor ordnen
		H = px3;
		px3 = px1;
		px1 = H;
		H = py3;
		py3 = py1;
		py1 = H;
		H = theta2;
		theta2 = theta1;
		theta1 = H;
		}
	
   Spitze_im_Polygon=punkt_in_polygon(px2, py2, start,feld);
	//Schnittpunkte Gerade 1 einf�gen
	
	j = start;
	testg = insideline(px1, py1, px2, py2, feld[j][1], feld[j][2]);
	j = (int)feld[start][4];
	s = j;
	
	do
		{
			testgn = insideline(px1, py1, px2, py2, feld[j][1], feld[j][2]);
			
			if ( testgn != testg )
			{
				schnittpunkt(px1, py1, px2, py2, feld[j][1], feld[j][2], feld[(int)feld[j][3]][1], feld[(int)feld[j][3]][2], xs, ys);
				einfuegen(j, xs, ys);
			}
			
			testg = testgn;
			j = (int)feld[j][4];
		}
		while (j != s);
		
		//Schnittpunkte Gerade 2 Einf�gen
		j = start;
		testg = insideline(px3, py3, px2, py2, feld[j][1], feld[j][2]);
		j = (int)feld[start][4];
		s = j;
		
		do
		{
			testgn = insideline(px3, py3, px2, py2, feld[j][1], feld[j][2]);
			
			if ( testgn != testg )
			{
				schnittpunkt(px3, py3, px2, py2, feld[j][1], feld[j][2], feld[(int)feld[j][3]][1], feld[(int)feld[j][3]][2], xs, ys);
				einfuegen(j, xs, ys);
			}
			
			testg = testgn;
			j = (int)feld[j][4];
		}
		while (j != s);
		
		//Punkte au�erhalb Sektor entfernen
		test0 = 1 - insidesektor(theta1, theta2, px2, py2, 0, 0, 0);	// wo liegt ursprung
		
		//Ersten Wert innerhalb suchen
		j = start;
		while (insidesektor(theta1, theta2, px2, py2, feld[j][1], feld[j][2], test0) == 0)
			j = (int)feld[j][4];
		
		loesch = 0;
		seingefuegt = 0;
		s = j;
		test = 1;
		j = (int)feld[j][4];
		
		do
				{
					testn = insidesektor(theta1, theta2, px2, py2, feld[j][1], feld[j][2], test0);
					jn = (int)feld[j][4];
					
					if ( testn == 0)
					{
						if ( s == j )
							s = jn;  //Muss das sein ???
						
						loesch = 1;
						Loeschen(j);
					}
					
					// Wenn Sektorspitze im Kreis diesen Punkt einf�gen
					if ( (testn != test) && (testn == 1) && (Spitze_im_Polygon==1) && (loesch == 1) )
					{
						seingefuegt = 1;
						einfuegen(j, px2, py2);
					}
					
					test = testn;
					j = jn;
				}
				while ( j != s );
				
				if ( (seingefuegt == 0) && (Spitze_im_Polygon==1) && (loesch == 1) )
					einfuegen(j, px2, py2);
}


//*******************************************************************************************



double  GrenzenSet::abstand(double x1, double y1, double x2, double y2)
/** Berechnet den Abstand von 2 Punkten (x1,y1), (x2,y2)
*------------------------------------------------------------------------------------------------*/
{
	return sqrt((x1 - x2) * (x1 - x2) + (y2 - y1) * (y2 - y1));
}


//*******************************************************************************************


void  GrenzenSet::Polygoninit(double rk)
/** Initialisiert Polygon als lineare Liste mit Vorg�nger und Nachfolger beginnend im
* Feld start (=0), mit dem Radius rk und frei Ecken
*------------------------------------------------------------------------------------------------*/
{
	double r, bg;
	int i;
	//===============================================================
	frei = nEck;
	r = sqrt(pi * rk * rk * 2 / (frei * sin(2 * pi / frei)));
	bg = 2 * pi / frei;
	
	for (i = 0; i <= (frei - 1); i++)
	{
				feld[i][1] = r * cos(i * bg);
				feld[i][2] = r * sin(i * bg);
				feld[i][3] = i - 1;
				feld[i][4] = i + 1;
				feld[i][5] = i;
				//TRACE2("\n%f5.2 %f5.2",feld[i][1], feld[i][2]);
	}
	
	feld[0][3] = frei - 1;
	feld[frei - 1][4] = 0;
}


//*******************************************************************************************


void  GrenzenSet::Loeschen(int index)
/** L�scht Punkt (index) aus verketteter Liste
//=============================================*/
{
	if ( index == start )
				start = (int)feld[index][4];
	
	feld[(int)feld[index][3]][4] = feld[index][4];
	feld[(int)feld[index][4]][3] = feld[index][3];
}


//*******************************************************************************************


void  GrenzenSet::einfuegen(int index, double x, double y/*, int frei*/)
/** F�gt an Stelle index den Punkt x,y in Polygon ein
*===================================================*/
{
	feld[frei][1] = x;
	feld[frei][2] = y;
	feld[frei][5] = frei;
	feld[frei][3] = feld[index][3];
	feld[frei][4] = feld[index][5];
	feld[(int)feld[index][3]][4] = frei;
	feld[(int)feld[index][5]][3] = frei;
	frei = frei + 1;
}


//*******************************************************************************************


int GrenzenSet::Punkt_in_Gerade(double px, double py, double x1, double y1, double x2, double y2,double &Abst,double  &mx,double  &my)
{
/**�berpr�ft ob Punkt P auf Gerade liegt
' 1 = Au�erhalb Abs gibt Abstand an
' 0 = auf Gerade
' -1 auf Gerade innerhalb der Punkte 1 und 2
' 0<=mx<=1 Punkt in Gerade mit Position mx=0 -> px=x1
'  0<=my<=1 Punkt in Gerade mit Position my=0 -> py=y1
'------------------------------------------------------------------
	*/
	double eps, vx, vy, wx, wy;
	int APG;
	
	
	//=================================================================
	eps = 0.0001f;
	APG = 1;
	
	if (abstand(x1, y1, x2, y2) < eps)  //Gerade ist Punkt
		{
		Abst = abstand(px, py, x1, x2);
    if (Abst < eps)  // Alle Punkte aufeinander
			{
			APG = -1;
      Abst = 0;
			}
		}
   else
		{
		if (fabs((py - y1) * (x2 - x1) - (y2 - y1) * (px - x1)) < eps)
			{
			
			if (fabs(x2 - x1) > eps) 
				my = (px - x1) / (x2 - x1);
			else if (fabs(y2 - y1) > eps) 
				my = (py - y1) / (y2 - y1);
			else
				my = 0;
    
    
			Abst = 0;
			APG = 0;     //   'Punkt liegt auf Linie
			if (my >= 0 && my <= 1)
				APG = -1; //   'Punkt liegt innerhalb der anderen Punkte
		}
		else
			{
			vx = x2 - x1;
			vy = y2 - y1;
			wx = px - x1;
			wy = py - y1;
			Abst = fabs(vx * wy - vy * wx);
			}
		}
		return  APG;
}

//*********************************************

int GrenzenSet::punkt_in_polygon(double x1, double y1, int start,double feld[][6])
	/**------------------------------------------------------------------------------
	* �berpr�ft ob der Punkt (x1.y1) im Polygonzug feld(.,.)liegt
	* 1= innerhalb, -1=au�erhalb, auf Rand=0
	* @param author B�sch	
	* @date 3.11.05 \n
	*--------------------------------------------------------------------------------*/
	{	
	int punkt_auf_rand=0, zwischen_den_punkten, punkt_in_poly=-1, j, icount=0,PiG;
	double Abst;
	double eps=0.000001, mx, my, x1s, y1s, xs, ys, tmp1;
	//=============================================================
	
	j = start;
	do
		{
		PiG = Punkt_in_Gerade(x1, y1, feld[j][1], feld[j][2], feld[(int)feld[j][4]][1], feld[(int)feld[j][4]][2], Abst, mx, my);
		if (PiG < 0)
			{
			punkt_auf_rand = 1;
			punkt_in_poly=0;
			}
		
		j = (int)feld[j][4];
		} while (j != start && punkt_auf_rand == 0);
	
		
		
		if (punkt_auf_rand == 0)
			{
			j = start;
			
			do 
				{
				x1s = x1;
				y1s = y1;
				if (fabs(x1s - feld[j][ 1]) < eps)
					x1s = x1s + eps;
				
				if (fabs(x1s - feld[(int)feld[j][4]][1]) < eps)
					x1s = x1s + eps;
				
				
				zwischen_den_punkten = (x1s > __min(feld[j][1], feld[(int)feld[j][4]][1])) && (x1s <= __max(feld[j][1], feld[(int)feld[j][4]][1]));
				if(zwischen_den_punkten )
					{
					tmp1 = schnittpunkt(x1s, y1s, x1s, y1s + 1, feld[j][1], feld[j][2], feld[(int)feld[j][4]][1], feld[(int)feld[j][4]][2], xs, ys);
				  if(tmp1==1 && ys >= y1s)
						icount++;
					}
				
				j = (int)feld[j][4];
			}	while (j != start); // Then GoTo weiter1
			

		if (icount % 2 !=0)
			punkt_in_poly = 1;   
		}

return punkt_in_poly;
}

//##############################################################################

// Routinen f�r die Exposition


void GrenzenSet::expos(float efeld[][2+1], short &enr,short &iErr)
	/**
	* 
	* --------------------------------------------------------------------------------------*/
	{
	
	double x ,y,xa,ya,xs,ys,wi,l,eps=0.000001;
  double s,abst,mx,my,xm,ym,xm1,ym1,dx,dy,nx,ny,tmp;	
	int g,j,i,ende; 

	//======================================================
  
	enr = -1;
    
	
   // Gibt es parallele Geraden, wenn ja dann entfernen  
		{ 
		int n,i,j, t,ungleich;
		double Abst,xs,ys;
        //--------------------- 
        n = 1;
        for (i = 2;i<=nGGerade;i++)
			{
            ungleich = 1;
            for (j = 1;j<= i - 1;j++)
				{
				s = schnittpunkt(fGGerade[i][1], fGGerade[i][2], fGGerade[i][3], fGGerade[i][4], fGGerade[j][1], fGGerade[j][2], fGGerade[j][3], fGGerade[j][4], xs, ys);
				if (s == 0)
					{
					t = Punkt_in_Gerade(fGGerade[i][1], fGGerade[i][2], fGGerade[j][1], fGGerade[j][2], fGGerade[j][3], fGGerade[j][4], Abst, mx, my);
					if (t <= 0) ungleich=0; //Geraden liegen aufeinander
					}
                 }
            if (ungleich == 1)   //Gerade entfernen
				{
                n = n + 1;
                fGGerade[n][1] = fGGerade[i][1];
				fGGerade[n][2] = fGGerade[i][2];
				fGGerade[n][3] = fGGerade[i][3];
				fGGerade[n][4] = fGGerade[i][4];
				}
			}
		if(n!=nGGerade)
			{
			iErr=-10;
			nGGerade=n;
			}
		}       
         

	x = feld[start][1];
	y = feld[start][2];
 
	//grenzensort(fe, gg, n);
	
	xa = x;
	ya = y;

	j = (int)feld[start][4];
	ende = j;
	marke:
			x = feld[j][1];
			y = feld[j][2];
        
			// Pr�fen ob Polygerade parallel zu Grenze
      if(fabs((x-xa)*(x-ya)+(y-ya)*(y-ya))>eps)  //Punkte m�ssen ungleich sein
				{
				for(g = 1; g<=nGGerade; g++)
					{
					s = schnittpunkt(xa, ya, x, y, fGGerade[g][1], fGGerade[g][2], fGGerade[g][3], fGGerade[g][4], xs, ys);
					if (s == 0)
						{
        
						i = Punkt_in_Gerade(x, y, fGGerade[g][1], fGGerade[g][2], fGGerade[g][3], fGGerade[g][4], abst, mx, my);
						 
						if (i <= 0)       //Geraden liegen aufeinander
							{  
							xm = (xa + x) / 2;
							ym = (ya + y) / 2;
							dx = (x - xa);
							dy = (y - ya);
							tmp = sqrt(dx * dx + dy * dy) * 10;
							dx = dx / tmp;
							dy = dy / tmp;
            
							xm1 = xm - dy;
							ym1 = ym + dx;
							i = punkt_in_polygon(xm1, ym1, start,feld);
							if (i<0)
								{
								nx =  xm1-xm;
								ny =  ym1-ym;
								}
							else
								{
								nx = xm - xm1;
								ny = ym - ym1;
								}
							
																		
							wi = atan2( ny,nx);
							wi = wi/pi*180.0f;
							if (wi<0) wi+=360;

							l = sqrt((x - xa)*(x-xa) + (y-ya)*(y-ya));
							if (l >= 0.00001) 
									{
									enr = enr + 1;
									//ASSERT(enr<9);
									efeld[enr][1] = (float)l;
									efeld[enr][2] = (float)wi;
									}
            
							}
						}
    
					}//Next g */
				}
			xa = x;
			ya = y;
			j = (int) feld[j][4];
	if (j != ende) goto marke;
	
}


//####################################################################

void GrenzenSet::grenzensort(double p[][2+1])
	{ 

	//=========================================
	/*
	nGGerade=0;
	for (i=1;i<=6;i++)
		{
		if(fabs(p[i][1]*p[i][1]+p[i][2]*p[i][2])>0.001f)
			{
			nGGerade++;
			fGGerade[nGGerade][1]=p[i][1];
		  fGGerade[nGGerade][2]=p[i][2];
			}
		}
	*/
	nGGerade=1;
	if(fabs(p[1][1])!=0.0f)
		{

		if (GLTyp[1]=="G")
			{
			fGGerade[1][1] = p[1][1];
			fGGerade[1][2] = p[1][2];
			fGGerade[1][3] = p[2][1];
			fGGerade[1][4] = p[2][2];
			nGGerade = 1;
			}
		else
			{       
			fGGerade[1][1] = p[1][1];
			fGGerade[1][2] = p[1][2];
			fGGerade[1][3] = p[2][1];
			fGGerade[1][4] = p[2][2];
          
			fGGerade[2][1] = p[1][1];
			fGGerade[2][2] = p[1][2];
			fGGerade[2][3] = p[3][1];
			fGGerade[2][4] = p[3][2];
			nGGerade = 2;
			}
		} 

  if(fabs(p[4][1])!=0.0f)
		{
		if (GLTyp[2]=="G")
			{
			nGGerade++;
			fGGerade[nGGerade][1] = p[4][1];
			fGGerade[nGGerade][2] = p[4][2];
			fGGerade[nGGerade][3] = p[5][1];
			fGGerade[nGGerade][4] = p[5][2];
			}   
		else      
			{
			nGGerade++;
			fGGerade[nGGerade][1] = p[4][1];
			fGGerade[nGGerade][2] = p[4][2];
			fGGerade[nGGerade][3] = p[6][1];
			fGGerade[nGGerade][4] = p[6][2];
  
			nGGerade++;
			fGGerade[nGGerade][1] = p[4][1];
			fGGerade[nGGerade][2] = p[4][2];
			fGGerade[nGGerade][3] = p[5][1];
			fGGerade[nGGerade][4] = p[5][2];
			}
		}
		
	}       


//*************************************************************************


