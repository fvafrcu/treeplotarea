/////////////////////////////////////////////////////////////////////////////
// Diese Datei ist Teil des Programmpakets "Betriebsinventur"
//																			 
// Copyright (C) 2001-2004 by Technologieberatung & SW-Entwicklung Helge Haug
/////////////////////////////////////////////////////////////////////////////



#pragma once



class GrenzenSet
{



private:


void ClippSektor(double px1, double py1, double px2, double py2, double px3, double py3);
void ClippGerade(double gx1, double gy1, double gx2, double gy2);
void Loeschen(int index);
void einfuegen(int index, double x, double y);

int insideline(double gx1, double gy1, double gx2, double gy2, double px, double py);
double abstand(double x1, double y1, double x2, double y2);
void GrenzenSet::grenzensort(double p[][2+1] );

public:
void Polygoninit(double rk);
double PFlaeche();
int insidesektor(double phi1, double phi2, double pxz, double pyz, double x, double y, double vergleich);
double theta(double p1x, double p1y, double p2x, double p2y);
void  GrenzenSet::BerechnePolygonflaeche(float fKe,float fKa, float fGrenze1[][3],float fGrenze2[][3], float Radius, double& Flaeche, double &dKfaktor, short &iErr);
int schnittpunkt(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double& xs, double& ys);   
int GrenzenSet::Punkt_in_Gerade(double px, double py, double x1, double y1, double x2, double y2,double &Abst,double  &mx,double  &my);
int GrenzenSet::punkt_in_polygon(double x1, double y1, int start, double feld[][6]);
int iLageBaum(double gx1, double gy1, double gx2, double gy2, double gx3, double gy3, double Bpx, double Bpy);
void GrenzenSet::expos(float efeld[][2+1], short &enr,short &iErr);
// Implementierung
};
