/** @file GrenzkreisDll.cpp
* Legt die Initialisierungsroutinen f�r die DLL fest.
* In dieser Datei sind alle Routinen, die in der DLL aufgerufen werden, enthalten
* - G_Kreisflaeche_BI
* - G_Kreisflaeche_BWI
* - SchnittpunktG
* - PunktInPolygon
* - LageBaum
* - Exposition
* - PolygonFlaeche
* - PolygonKreis
* - Polygonausgabe
* - setNEck

* - Routinen f�r die Statistiksoftware R
* - R_G_Kreisflaeche_BWI
* - R_SchnittpunktG
* - R_PunktInPolygon
* - R_Exposition
* - R_LageBaum
* - R_Polygonausgabe
*
*
*-----------------------------------------------------------------------------------------*/




#include "stdafx.h"
#include "GrenzkreisDll.h"
#include "poly.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern double feld[][5+1];
extern int start;
extern int frei;
extern int nEck;
extern double tx,ty;

GrenzenSet GR;

//
//	Beachten Sie!
//
//		Wird diese DLL dynamisch an die MFC-DLLs gebunden,
//		muss bei allen von dieser DLL exportierten Funktionen,
//		die MFC-Aufrufe durchf�hren, das Makro AFX_MANAGE_STATE
//		direkt am Beginn der Funktion eingef�gt sein.
//
//		Beispiel:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//		// Hier normaler Funktionsrumpf
//		}
//
//		Es ist sehr wichtig, dass dieses Makro in jeder Funktion
//		vor allen MFC-Aufrufen erscheint. Dies bedeutet, dass es
//		als erste Anweisung innerhalb der Funktion ausgef�hrt werden
//		muss, sogar vor jeglichen Deklarationen von Objektvariablen,
//		da ihre Konstruktoren Aufrufe in die MFC-DLL generieren
//		k�nnten.
//
//		Siehe MFC Technical Notes 33 und 58 f�r weitere
//		Details.
//

/////////////////////////////////////////////////////////////////////////////
// CGrenzkreisDllApp

BEGIN_MESSAGE_MAP(CGrenzkreisDllApp, CWinApp)
	//{{AFX_MSG_MAP(CGrenzkreisDllApp)
	// HINWEIS - Hier werden Mapping-Makros vom Klassen-Assistenten eingef�gt und entfernt.
	//    Innerhalb dieser generierten Quelltextabschnitte NICHTS VER�NDERN!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGrenzkreisDllApp Konstruktion

CGrenzkreisDllApp::CGrenzkreisDllApp()
	{
	// ZU ERLEDIGEN: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance platzieren
	}

/////////////////////////////////////////////////////////////////////////////
// Das einzige CGrenzkreisDllApp-Objekt

CGrenzkreisDllApp theApp;

//************************************************************************************************
void transform_BWIzuBI(float fGrenze1[][3],float fGrenze2[][3],float fG1[][3], float fG2[][3]);




//************************************************************************************************
void __stdcall setNEck(short &setnEck)
	{
	/**=======================================================================================================================
	* Definiert die Anzahl der Ecken, mit welchen der Probekreis durch ein
	* Polynom repr�sentiert wird. Default ist 40		
	* @param setnEck: Anzahl Polynomecken
	* @author:	B�sch
	* ----------------------------------------------------------------------------------*/
	nEck=min(setnEck,maxfeld);
	nEck=max(setnEck,3);
	}




//*************************************************************************************************



double __stdcall G_Kreisflaeche_BI	(float fGrenze1[][3],float fGrenze2[][3], float *Radius, float &fKfaktor, short &iErr)
	/**=======================================================================================================================
	* Diese Funktion wurde erstellt, um die DLL auch f�r die Nutzung innerhalb des 
	* Betriebsinventur-Programms verwenden zu k�nnen, in welchem die Abspeicherung der Grenzlinien 
	* in einem von der BWI abweichendem Schema erfolgt. 
	* Die Funktion liefert die Fl�che des von den Bestandesgrenzen geschnittenen Stichprobenkreises.
	* Die Felder Grenze1 bzw. Grenze2 enthalten die eingemessenen Grenzen des Probekreises 
	* (BI-konform), die  entweder als Gerade oder als Polygonzug von 2 Geraden vorliegen (Sektoren).
	* Die Sektorspitze liegt im ersten Feld Grenz1(1,.) bzw. Grenz2(1,.). Der Radius entspricht dem 
	* Radius des Probekreises.

	* @param fGrenze1: Grenzen 1
	* @param fGrenze2: Grenzen 2
	* @param fRadius: Radius des Probekreises
	* @Return : KFaktor, Verh�ltnis von RestFl�chen zur Gesamtfl�che des Kreises
	* @Return : iFehler, siehe Benutzeranleitung
	* @author:	B�sch
	---------------------------------------------------------------------------------------------------------------------------------------------------*/
	{
	double Flaeche;
	double dKfaktor;
	float *t;
	t=&fGrenze1[0][0];
	Flaeche=0;
	GR.BerechnePolygonflaeche(0.0f, 0.0f, fGrenze1,fGrenze2, *Radius, Flaeche, dKfaktor,iErr);
	fKfaktor=(float)dKfaktor;
	return Flaeche;
	}

//*************************************************************************************************

double __stdcall G_Kreisflaeche_BWI	(float *fEntf, float *fAzi, float fGrenze1[][3],float fGrenze2[][3], float *fBhd, float &fKfaktor, short &iErr)
	/**================================================================================================================================================
	* Die Funktion G_Kreisfl�che_BWI berechnet die Fl�che des Kreises mit Mittelpunkt Entf, Azimut
	* mit den Grenzen Grenze1, Grenze2. Die Sektorspitze liegt im zweiten Feld Grenze1(1,.) 
	* bzw. Grenze2(1,.) (Indizierung beginnt bei 0!).  Dieses Feld  bleibt frei, wenn es sich um eine 
	* Gerade handelt.
	* Die Prozedur kann verwendet werden f�r Probekreise (Entf=0.0, fAzi=0.0, Radius=Radius des Probekreises) 
	* oder f�r den Probekreis um den Einzelbaum. In diesem Falle entspricht Entf, Azimut den Koordinaten des 
	* Einzelbaumes, der Radius dem Wert (Bhd/(2*Wurzel(Z�hlbreite)). 

	* Entf:	 	Koordinaten des Einzelbaumes (m), bei Probekreis =0
	* Azimut:	Koordinate des Einzelbaumes (gon), bei Probekreis=0
	* fGrenze.[0][i]= i=0-2, Azimut  fGrenze.[1][i]; i=0-2; Entfernung
	*
	* @param Grenze1(0,0)	Azimut (gon) erster Punkt		
	* @param Grenze1(1,0)	Entfernung (m) erster Punkt		
	* @param Grenze1(0,1) 	Azimut (gon) zweiter Punkt		 /Spitze bei Knick
	* @param Grenze1(1,1) 	Entfernung (m) zweiter Punkt	/ Spitze bei Knick
	* @param Grenze1(0,2) 	Azimut (gon) dritter Punkt
	* @param Grenze1(1,2) 	Entfernung (m) dritter Punkt

	* @param Grenze2(. ,. ) 	Azimut(gon) erster Punkt
	* @param   	. 
	* @param      	.
	* @param Radius		Radius des Probekreises (m)

	* Output: 	Funktionswert: Schnittfl�che [m�]
	* @param Kfaktor: 	Fl�che voller Grenzkreis/Fl�che Geschnittener Grenzkreiskreis
	* @param iErr: 		siehe 4.1 Benutzeranleitung
	* -----------------------------------------------------------------------------------------*/

	{
	double Flaeche, dKfaktor;

	float fG1t[2][3],fG2t[2][3];

	//==================================================================================================

	iErr=0;

	transform_BWIzuBI(fGrenze1, fGrenze2,fG1t,fG2t);
	GR.BerechnePolygonflaeche(*fEntf,*fAzi, fG1t,fG2t, *fBhd, Flaeche,dKfaktor,iErr);
	fKfaktor=(float)(dKfaktor);
	return Flaeche;
	}





/*****************************************************************************************/
short _stdcall SchnittpunktG(double *x1, double *y1, double *x2, double *y2, double *x3, double *y3, double *x4, double *y4, double& xs, double& ys)
	/**Diese Funktion bestimmt den Schnittpunkt (xs, ys) von zwei Geraden. 
	* Gerade 1 ist durch die Punkte g1x1, g1y1, g1x2, g1y2 gegeben, Gerade 2 durch g2x3, g2y3 
	* und g2x4, g2y4. Der R�ckgabewert ist 1 wenn ein Schnittpunkt existiert, in diesem Falle 
	* sind die Koordinaten des Schnittpunktes (xs,ys). Wenn die Geraden parallel sind ist der 
	* R�ckgabewert 0.
	*-------------------------------------------------------------------------------------*/

	{

	return (short) GR.schnittpunkt(*x1, *y1, *x2, *y2, *x3, *y3, *x4, *y4, xs, ys);

	}

/*******************************************************************************************/

short _stdcall PunktInPolygon(double &x1, double &y1, short &n, double *px, double *py)
	/**-----------------------------------------------------------------------------------
	*' �berpr�ft ob Punkt (x1.y1) im Polygonzug px(0:n-1),py(0-n1) liegt
	* ' 1= innerhalb, -1=au�erhalb, Rand=0
	*-------------------------------------------------------------------------------------*/
	{	
	int i;
	double (*feld)[6];
	feld=new double[n][6];
	//====================================================================================

	for (i = 0; i <= (n - 1); i++)
		{
		feld[i][1] = px[i];
		feld[i][2] = py[i];
		feld[i][3] = i - 1;
		feld[i][4] = i + 1;
		feld[i][5] = i;
		}

	feld[0][3] = n - 1;
	feld[n - 1][4] = 0;

	i=GR.punkt_in_polygon(x1, y1, 0,feld);
	delete(feld);
	return (short) i;

	}

//************************************************************************************************


short _stdcall LageBaum(double &gx1, double &gy1, double &gx2, double &gy2, double &gx3, double &gy3, double &Bpx, double &Bpy)
	/**------------------------------------------------------------------------------------------------
	* Funktion stellt fest, ob ein Baum mit den Koordinaten Bpx und Bpy au�erhalb von Grenzlinien liegt
	*  bezogen auf den Ursprung (= Stichprobenmittelpunkt)
	*  R�ckgabewert 1: Baum ist im Probekreis, 0: Baum ist au�erhalb
	*  Es werden zwei F�lle unterschieden:
	*  Die Grenzlinie ist eine Gerade (gx2, gxy ["Knickpunkt"]ist jeweils 0)
	*  Die Grenzlinie ist geknickt
	*  Erl�uterung: dieses Kriterium, Knickpunkt Koordinaten = 0 ist ausreichend, da verfahrensgem�� eine
	* linie nicht durch den Stichprobenmittelpunkt gehen darf.
	* Bem B�sch: kann inzwischen einfacher durch Routine Punkt_in_Polygon gel�st werden, wenn man f�r das Polygon den 
	* resultierenden Polygonzug nimmt
	* @param author G. K�ndler
	* @param date 02/2002\n
	*---------------------------------------------------------------------------------------------------------------*/

	{
	return GR.iLageBaum(gx1, gy1, gx2, gy2, gx3, gy3, Bpx, Bpy);
	}

//*******************************************************************************************************************************************

short _stdcall test(short &n)
	{
	float a;
	a=1;
	return 1;
	}

//***********************************************************************************************

short _stdcall PolygonKreis(double &rk)
	/** erstellt ein Polygon mit Radius rk*/

	{
	GR.Polygoninit(rk);
	return 1;
	}

	//***********************************************************************************************

	double _stdcall PolygonFlaeche()
	//**Berechnet die Fl�che innerhalb des Polygons
	//------------------------------------------------------------------------------------------------*/

	{
	return GR.PFlaeche();
	}



//***************************************************************************************
void _stdcall Exposition(float *fEntf, float *fAzi, float fGrenze1[][3],float fGrenze2[][3], float *Radius, float *winkel, float *laenge, short &enr, short &iErr)
	{
	/** ===============================================================================================\n 
	Die Funktion liefert f�r den Kreis mit dem Mittelpunkt Entf, Azi die Expositionswinkel und die L�nge
	* der Kanten an den Bestandesgrenzen. Der Anwender kann selbst�ndig entscheiden, ob er nur die Waldr�nder 
	* betrachten will (Rk=1 oder 2), die Bestandesr�nder (Rk=3 oder 4) oder beide zusammen, indem er nur die 
	* entsprechenden Grenzen beim Aufruf �bergibt. Die Ergebnisse sind je nach Aufruf unterschiedlich, da 
	* sich die einzelnen Grenzen �berschneiden oder gegenseitig beeinflussen k�nnen.
	* 
	* @param fEntf : Entfernung des Kreises vom Mittelpunkt Aufnahme
	* @param fAzi  : Azimut
	* @param fGrenze1[][3]: siehe Def. BWI,
	* @param fGrenze2[][3]: siehe Def. BWI, 
	* @return Radius : zur Exp.Linie, 
	* @return winkel : Winkel der Exp. 
	* @return laenge : L�nge der Exp. 
	* @return enr	 : durchlaufende Nummerierung 
	* @return iErr   : Fehler siehe Benutzeranleitung
	* @author Boesch\n
	*----------------------------------------------------------------------------------------------------*/

	//static const int exposdim=50;
	float exposition[exposdim+1][2+1];
	double Flaeche,dKfaktor;
	int i;
	float fG1t[2][3],fG2t[2][3];

	//==================================================================================================
	iErr=0;
	//if (fGrenze1[0][0]==100.0f && fGrenze1[0][1]==0.0f &&fGrenze1[0][2]==238.f)
	//	enr=enr;

	transform_BWIzuBI(fGrenze1, fGrenze2,fG1t,fG2t);
	GR.BerechnePolygonflaeche(*fEntf,*fAzi, fG1t,fG2t, *Radius, Flaeche,dKfaktor,iErr);	
	if(iErr==0 || iErr<=-10)
		{
		GR.expos(exposition, enr,iErr);

		for(i=0;i<=enr && i<exposdim && i<6;i++)
			{
			laenge[i]=exposition[i][1];
			winkel[i]=exposition[i][2];
			winkel[i]=(360-winkel[i])/360*400+100; // Umrechnung Gon
			if (winkel[i]>=400) winkel[i] -= 400.0;
			}
		enr++;
		if(enr>6 ) 
			{
			iErr=-6; 
			enr=6;
			}
		}

	//if(enr>6) enr=6;    // Da externes Feld mit 6 dimensioniert
	}

//*****************************************************************************************

void _stdcall Polygonausgabe(double *px, double *py, short &snEck, short &ndim, short &iErr)
	/**
	* Liefert die Koordinaten des resultierenden Polygons mit nEck Ecken in den Feldern px, py.
	* Bem. Die Dimensionierung im aufrufenden Programm muss ausreichend sein, als grobe 
	* Richtlinie gilt bei einer Aufl�sung von n (setnEck(n)) n*1.2. Bei der default Aufl�sung 
	* von n=40 sind ergibt das eine Dimension von 48. Drehrichtung gegen Uhrzeigersinn
	* @param author Boesch
	*-----------------------------------------------------------------------------------------*/
	{

	int j;
	//============================================================
	iErr=0;
	j = start;
	snEck=0;
	do
		{
		snEck++;
		if(snEck<=ndim)
			{
			px[snEck-1]=feld[j][1]+tx;
			py[snEck-1]=feld[j][2]+ty;
			}
		j=(int)feld[j][4];
		}
		while (j!= start);
		if(snEck>ndim)
			{
			iErr=-5;
			snEck=ndim;
			}
	}




/**********************************************************************************************
/**************************** R Exporte *******************************************************
************************************************************************************************/

void _cdecl R_SchnittpunktG(double *x1, double *y1, double *x2, double *y2, double *x3, double *y3, double *x4, double *y4, double& xs, double& ys, int &iRet)
	//Bestimmt (xs,ys) als Schnittpunkt von zwei Geraden, gegeben durch die Punkte
	//gx1,g1y1,g1x2,g1y2,...,g2x1...; wert = 0 wenn geraden parallel)
	{

	iRet= (short) GR.schnittpunkt(*x1, *y1, *x2, *y2, *x3, *y3, *x4, *y4, xs, ys);

	}



void _cdecl R_PunktInPolygon(double &x1, double &y1, short &n, double *px, double *py, int &i)
	{	
	i = PunktInPolygon(x1, y1, n, px, py);
	}

//********************************************************************************************************************************************

void _cdecl R_Exposition(float *fEntf, float *fAzi, float fGrenze1[][3],float fGrenze2[][3], float *Radius, float *winkel, float *laenge, short &enr, int &iErr)
	{
	short itmp;
	itmp=0;
	Exposition(fEntf, fAzi, fGrenze1, fGrenze2, Radius, winkel, laenge,enr, itmp);
	iErr=itmp;
	}

//*****************************************************************************************************************************************************************

void _cdecl R_G_Kreisflaeche_BWI	(float *fEntf, float *fAzi, float fGrenze1[][3],float fGrenze2[][3], float *fBhd, float &fKfaktor, float &Flaeche,int &iErr)
	{

	short itmp;
	itmp=0;
	Flaeche=(float)G_Kreisflaeche_BWI	(fEntf, fAzi, fGrenze1,fGrenze2, fBhd, fKfaktor, itmp);
	iErr=itmp;
	}

//*******************************************************************************************************************************************

void _cdecl R_LageBaum(double &gx1, double &gy1, double &gx2, double &gy2, double &gx3, double &gy3, double &Bpx, double &Bpy, int &L)
	{

	L= GR.iLageBaum(gx1, gy1, gx2, gy2, gx3, gy3, Bpx, Bpy);
	}

//**********************************************************************************************

void _cdecl R_Test(double &gx1, double &gy1)
	{
	gx1=3;
	gy1=5;

	}

//****************************************************************************************
void _cdecl R_Polygonausgabe(double *px, double *py, int &nEck)
	{

	int j;
	//============================================================
	j = start;
	nEck=0;

	do
		{
		nEck++;
		px[nEck-1]=feld[j][1]+tx;
		py[nEck-1]=feld[j][2]+ty;
		j=(int)feld[j][4];
		}
		while (j!= start);

	}


/************************************************************************************************
*************************************************************************************************
*******************  Hilfsroutinen **************************************************************/



void transform_BWIzuBI(float fGrenze1[][3],float fGrenze2[][3], float fG1[][3], float fG2[][3])
	// Transformiert die BWI Darstellung (Sektorspitze = mittlerer Punkt) in die BI Darstellung (Sektorspitze =  1. Punkt)
	{
	//==========================================================================================

	if (fGrenze1[1][1]>0)
		{
		fG1[0][0]=fGrenze1[0][1];
		fG1[1][0]=fGrenze1[1][1];
		fG1[0][1]=fGrenze1[0][0];
		fG1[1][1]=fGrenze1[1][0];
		fG1[0][2]=fGrenze1[0][2];
		fG1[1][2]=fGrenze1[1][2];
		}
	else
		{
		fG1[0][0]=fGrenze1[0][0];
		fG1[1][0]=fGrenze1[1][0];
		fG1[0][1]=fGrenze1[0][2];
		fG1[1][1]=fGrenze1[1][2];
		fG1[0][2]=fGrenze1[0][1];
		fG1[1][2]=fGrenze1[1][1];
		}

	if (fGrenze2[1][1]>0)
		{
		fG2[0][0]=fGrenze2[0][1];
		fG2[1][0]=fGrenze2[1][1];
		fG2[0][1]=fGrenze2[0][0];
		fG2[1][1]=fGrenze2[1][0];
		fG2[0][2]=fGrenze2[0][2];
		fG2[1][2]=fGrenze2[1][2];
		}
	else
		{
		fG2[0][0]=fGrenze2[0][0];
		fG2[1][0]=fGrenze2[1][0];
		fG2[0][1]=fGrenze2[0][2];
		fG2[1][1]=fGrenze2[1][2];
		fG2[0][2]=fGrenze2[0][1];
		fG2[1][2]=fGrenze2[1][1];
		}


	}
